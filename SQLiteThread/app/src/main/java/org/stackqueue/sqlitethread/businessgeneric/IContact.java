package org.stackqueue.sqlitethread.businessgeneric;

import org.stackqueue.sqlitethread.databasegeneric.Contact;

import java.util.ArrayList;

public interface IContact {
    Contact fetchContact(int contactPk);

    ArrayList<Contact> fetchContactCollection();

    long insertContact(Contact contact);

    int updateContact(Contact contact);

    int deleteContact(Contact contact);

    long[] insertContactCollection(ArrayList<Contact> contactCollection);

    int[] updateContactCollection(ArrayList<Contact> contactCollection);

    int[] deleteContactCollection(ArrayList<Contact> contactCollection);
}
