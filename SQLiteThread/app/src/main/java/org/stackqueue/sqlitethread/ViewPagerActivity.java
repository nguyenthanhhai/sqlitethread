package org.stackqueue.sqlitethread;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;

import java.util.List;

public class ViewPagerActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        if ((ActivityCompat.checkSelfPermission(ViewPagerActivity.this.getApplicationContext(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(ViewPagerActivity.this.getApplicationContext(), Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(ViewPagerActivity.this, new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_CONTACTS}, 0x00);
        }

        ViewPager view_pager = (ViewPager) findViewById(R.id.view_pager);
        view_pager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for (Fragment fragment : fragments) {
            if (fragment instanceof MessageFragment) {
                MessageFragment messageFragment = (MessageFragment) fragment;
                Bundle bundle = intent.getBundleExtra("NTH_BUNDLE");
                messageFragment.setOnUiArguments(bundle);
            }
            else {
                if (fragment instanceof ContactFragment) {
                    ContactFragment contactFragment = (ContactFragment) fragment;
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
