package org.stackqueue.sqlitethread;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.stackqueue.sqlitethread.databasegeneric.Contact;

import java.util.ArrayList;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder> {
    private Context context;
    private ArrayList<Contact> contactCollection;
    private Contact contact;

    public ContactAdapter(Context context, ArrayList<Contact> contactCollection) {
        this.context = context;
        this.contactCollection = contactCollection;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ContactViewHolder contactViewHolder = new ContactViewHolder(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.contact_view_holder, viewGroup, false));

        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        contact = contactCollection.get(position);
        holder.text_view_contact_name.setText(contact.getContactName());
        holder.text_view_phone_number.setText(contact.getPhoneNumber());
        holder.image_view_photo.setImageURI(contact.getPhoto());
    }

    @Override
    public int getItemCount() {
        return contactCollection.size();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder {
        private TextView text_view_contact_name;
        private TextView text_view_phone_number;
        private ImageView image_view_photo;

        public ContactViewHolder(View view) {
            super(view);

            text_view_contact_name = (TextView) view.findViewById(R.id.text_view_contact_name);
            text_view_phone_number = (TextView) view.findViewById(R.id.text_view_phone_number);
            image_view_photo = (ImageView) view.findViewById(R.id.image_view_photo);
        }
    }
}
