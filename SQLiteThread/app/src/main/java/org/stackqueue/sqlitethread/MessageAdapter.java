package org.stackqueue.sqlitethread;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.stackqueue.sqlitethread.businessgeneric.IMessage;
import org.stackqueue.sqlitethread.businessspecific.TMessage;
import org.stackqueue.sqlitethread.databasegeneric.Message;

import java.util.ArrayList;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private Context context;
    private ArrayList<Message> messageCollection;
    private Message message;

    public MessageAdapter(Context context, ArrayList<Message> messageCollection) {
        this.context = context;
        this.messageCollection = messageCollection;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        MessageViewHolder messageViewHolder = new MessageViewHolder(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.message_view_holder, viewGroup, false));

        return messageViewHolder;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        message = messageCollection.get(position);
        holder.text_view_contact_name.setText(message.getContactName());
        holder.text_view_phone_number.setText(message.getPhoneNumber());
        holder.text_view_message.setText(message.getMessage());
        holder.image_view_photo.setImageURI(message.getPhoto());
    }

    @Override
    public int getItemCount() {
        return messageCollection.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private TextView text_view_contact_name;
        private TextView text_view_phone_number;
        private TextView text_view_message;
        private ImageView image_view_photo;

        public MessageViewHolder(View view) {
            super(view);

            text_view_contact_name = (TextView) view.findViewById(R.id.text_view_contact_name);
            text_view_phone_number = (TextView) view.findViewById(R.id.text_view_phone_number);
            text_view_message = (TextView) view.findViewById(R.id.text_view_message);
            image_view_photo = (ImageView) view.findViewById(R.id.image_view_photo);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            IMessage iMessage = new TMessage(context);
                            long insert = iMessage.insertMessage(message);
                            Log.i("NTH", String.valueOf(insert));
                        }
                    }).start();

                    Intent intentMessageDetails = new Intent(context, MessageDetailsActivity.class);
                    intentMessageDetails.putExtra("NTH_CONTACT_NAME_DETAILS", message.getContactName());
                    intentMessageDetails.putExtra("NTH_PHONE_NUMBER_DETAILS", message.getPhoneNumber());
                    intentMessageDetails.putExtra("NTH_MESSAGE_DETAILS", message.getMessage());
                    intentMessageDetails.putExtra("NTH_PHOTO_DETAILS", message.getPhoto().toString());
                    context.startActivity(intentMessageDetails);
                }
            });
        }
    }
}
