package org.stackqueue.sqlitethread;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.stackqueue.sqlitethread.databasegeneric.Contact;

import java.util.ArrayList;

public class ContactFragment extends Fragment {
    private ArrayList<Contact> contactCollection;
    private ContactAdapter contactAdapter;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        super.onCreateView(layoutInflater, viewGroup, savedInstanceState);

        return layoutInflater.inflate(R.layout.fragment_contact, viewGroup, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        contactCollection = new ArrayList<Contact>();
        final Cursor cursor = ContactFragment.this.getContext().getContentResolver()
                .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                        String phonenumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                        contactCollection.add(new Contact(contactName, phonenumber, Uri.parse(photoUri)));
                    }
                });
            } while (cursor.moveToNext());
        }
        cursor.close();

        RecyclerView recycler_view_contact = (RecyclerView) view.findViewById(R.id.recycler_view_contact);
        recycler_view_contact.setHasFixedSize(true);

        LinearLayoutManager linear_layout_manager_contact = new LinearLayoutManager(ContactFragment.this.getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_view_contact.setLayoutManager(linear_layout_manager_contact);

        DividerItemDecoration divider_item_decoration_contact = new DividerItemDecoration(ContactFragment.this.getContext(), LinearLayoutManager.VERTICAL);
        recycler_view_contact.addItemDecoration(divider_item_decoration_contact);

        contactAdapter = new ContactAdapter(ContactFragment.this.getContext(), contactCollection);
        recycler_view_contact.setAdapter(contactAdapter);
    }
}
