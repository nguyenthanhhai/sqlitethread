package org.stackqueue.sqlitethread.businessspecific;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.stackqueue.sqlitethread.businessgeneric.IContact;
import org.stackqueue.sqlitethread.databasegeneric.Contact;
import org.stackqueue.sqlitethread.databasespecific.DatabaseOpenHelper;

import java.util.ArrayList;

public class TContact extends DatabaseOpenHelper implements IContact {
    public TContact(Context context) {
        super(context);
    }

    public TContact(Context context, String name) {
        super(context, name);
    }

    public TContact(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory) {
        super(context, name, cursorFactory);
    }

    public TContact(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public Contact fetchContact(int contactPk) {
        Contact contact = new Contact();

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.query("contacts", null, "contact_pk = ?", new String[]{String.valueOf(contactPk)}, null, null, null, null);
            if (cursor.moveToFirst()) {
                contact.setContactPk(cursor.getInt(cursor.getColumnIndex("contact_pk")));
                contact.setContactName(cursor.getString(cursor.getColumnIndex("contact_name")));
                contact.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phone_number")));
            }
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return contact;
    }

    @Override
    public ArrayList<Contact> fetchContactCollection() {
        ArrayList<Contact> contactCollection = new ArrayList<Contact>();

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.query("contacts", null, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Contact contact = new Contact();
                    contact.setContactPk(cursor.getInt(cursor.getColumnIndex("contact_pk")));
                    contact.setContactName(cursor.getString(cursor.getColumnIndex("contact_name")));
                    contact.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phone_number")));
                    contactCollection.add(contact);
                } while (cursor.moveToNext());
            }
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return contactCollection;
    }

    @Override
    public long insertContact(Contact contact) {
        long insert = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("contact_name", contact.getContactName());
            contentValues.put("phone_number", contact.getPhoneNumber());
            insert = sqLiteDatabase.insert("contacts", null, contentValues);
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return insert;
    }

    @Override
    public int updateContact(Contact contact) {
        int update = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("contact_name", contact.getContactName());
            contentValues.put("phone_number", contact.getPhoneNumber());
            update = sqLiteDatabase.update("contacts", contentValues, "contact_pk = ?", new String[]{String.valueOf(contact.getContactPk())});
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return update;
    }

    @Override
    public int deleteContact(Contact contact) {
        int delete = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            delete = sqLiteDatabase.delete("contacts", "contact_pk = ?", new String[]{String.valueOf(contact.getContactPk())});
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return delete;
    }

    @Override
    public long[] insertContactCollection(ArrayList<Contact> contactCollection) {
        long[] inserts = new long[contactCollection.size()];

        try {
            for (Contact contact : contactCollection) {
                inserts[contactCollection.indexOf(contact)] = insertContact(contact);
            }
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return inserts;
    }

    @Override
    public int[] updateContactCollection(ArrayList<Contact> contactCollection) {
        int[] updates = new int[contactCollection.size()];

        try {
            for (Contact contact : contactCollection) {
                updates[contactCollection.indexOf(contact)] = updateContact(contact);
            }
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return updates;
    }

    @Override
    public int[] deleteContactCollection(ArrayList<Contact> contactCollection) {
        int[] deletes = new int[contactCollection.size()];

        try {
            for (Contact contact : contactCollection) {
                deletes[contactCollection.indexOf(contact)] = deleteContact(contact);
            }
        } catch (Exception exception) {
            Log.i("TContact", exception.getMessage());
        }

        return deletes;
    }
}
