package org.stackqueue.sqlitethread.businessgeneric;

import org.stackqueue.sqlitethread.databasegeneric.Message;

import java.util.ArrayList;

public interface IMessage {
    Message fetchMessage(int messagePk);

    ArrayList<Message> fetchMessageCollection();

    long insertMessage(Message message);

    int updateMessage(Message message);

    int deleteMessage(Message message);

    long[] insertMessageCollection(ArrayList<Message> messageCollection);

    int[] updateMessageCollection(ArrayList<Message> messageCollection);

    int[] deleteMessageCollection(ArrayList<Message> messageCollection);
}
