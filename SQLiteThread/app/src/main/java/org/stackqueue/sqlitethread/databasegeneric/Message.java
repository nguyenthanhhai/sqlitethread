package org.stackqueue.sqlitethread.databasegeneric;

import android.net.Uri;

public class Message {
    private int messagePk;
    private String contactName;
    private String phoneNumber;
    private String message;

    private Uri photo;

    public Message() {
    }

    public Message(int messagePk, String contactName, String phoneNumber, String message) {
        this.messagePk = messagePk;
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.message = message;
    }

    public Message(String contactName, String phoneNumber, String message, Uri photo) {
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.message = message;
        this.photo = photo;
    }

    public int getMessagePk() {
        return messagePk;
    }

    public void setMessagePk(int messagePk) {
        this.messagePk = messagePk;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }
}
