package org.stackqueue.sqlitethread.databasegeneric;

import android.net.Uri;

public class Contact {
    private int contactPk;
    private String contactName;
    private String phoneNumber;
    private Uri photo;

    public Contact() {
    }

    public Contact(int contactPk, String contactName, String phoneNumber) {
        this.contactPk = contactPk;
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
    }

    public Contact(String contactName, String phoneNumber, Uri photo) {
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.photo = photo;
    }

    public int getContactPk() {
        return contactPk;
    }

    public void setContactPk(int contactPk) {
        this.contactPk = contactPk;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Uri getPhoto() {
        return photo;
    }

    public void setPhoto(Uri photo) {
        this.photo = photo;
    }
}
