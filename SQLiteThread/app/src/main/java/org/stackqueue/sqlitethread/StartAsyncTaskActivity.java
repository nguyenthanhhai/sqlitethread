package org.stackqueue.sqlitethread;

import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StartAsyncTaskActivity extends AppCompatActivity {
    private TextView text_view_run;
    private TextView text_view_msg;
    private Button button_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_async_task);

        text_view_run = (TextView) findViewById(R.id.text_view_run);
        text_view_msg = (TextView) findViewById(R.id.text_view_msg);
        button_start = (Button) findViewById(R.id.button_start);

        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartAsyncTask startAsyncTask = new StartAsyncTask();
                startAsyncTask.execute();
            }
        });
    }

    public class StartAsyncTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            text_view_msg.setText("Start");
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int index = 0; index < 101; index++) {
                publishProgress(index);
                SystemClock.sleep(50);
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            int index = values[0];
            Log.i("NTH", String.valueOf(index));

            text_view_run.setText(String.valueOf(index));
            text_view_msg.setText(String.valueOf(index) + " % ");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            text_view_msg.setText("Finish");
        }
    }
}
