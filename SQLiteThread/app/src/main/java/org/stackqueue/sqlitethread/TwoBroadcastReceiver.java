package org.stackqueue.sqlitethread;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class TwoBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("org.stackqueue.sqlitethread.ACTION_MESSAGE")) {
            String message = intent.getStringExtra("NTH");
            Log.i("NTH", message);
        }
    }
}
