package org.stackqueue.sqlitethread;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageDetailsActivity extends AppCompatActivity {
    private TextView text_view_contact_name_details;
    private TextView text_view_phone_number_details;
    private TextView text_view_message_details;
    private ImageView image_view_photo_details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);

        text_view_contact_name_details = (TextView) findViewById(R.id.text_view_contact_name_details);
        text_view_phone_number_details = (TextView) findViewById(R.id.text_view_phone_number_details);
        text_view_message_details = (TextView) findViewById(R.id.text_view_message_details);
        image_view_photo_details = (ImageView) findViewById(R.id.image_view_photo_details);

        Intent intentDetails = getIntent();
        text_view_contact_name_details.setText(intentDetails.getStringExtra("NTH_CONTACT_NAME_DETAILS"));
        text_view_phone_number_details.setText(intentDetails.getStringExtra("NTH_PHONE_NUMBER_DETAILS"));
        text_view_message_details.setText(intentDetails.getStringExtra("NTH_MESSAGE_DETAILS"));
        image_view_photo_details.setImageURI(Uri.parse(intentDetails.getStringExtra("NTH_PHOTO_DETAILS")));
    }
}
