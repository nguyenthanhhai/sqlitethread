package org.stackqueue.sqlitethread;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.stackqueue.sqlitethread.databasegeneric.Message;

import java.util.ArrayList;

public class MessageFragment extends Fragment {
    private ArrayList<Message> messageCollection;
    private MessageAdapter messageAdapter;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        super.onCreateView(layoutInflater, viewGroup, savedInstanceState);

        return layoutInflater.inflate(R.layout.fragment_message, viewGroup, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        messageCollection = new ArrayList<Message>();

        RecyclerView recycler_view_message = (RecyclerView) view.findViewById(R.id.recycler_view_message);
        recycler_view_message.setHasFixedSize(true);

        LinearLayoutManager linear_layout_manager_message = new LinearLayoutManager(MessageFragment.this.getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_view_message.setLayoutManager(linear_layout_manager_message);

        DividerItemDecoration divider_item_decoration_message = new DividerItemDecoration(MessageFragment.this.getContext(), LinearLayoutManager.VERTICAL);
        recycler_view_message.addItemDecoration(divider_item_decoration_message);

        messageAdapter = new MessageAdapter(MessageFragment.this.getContext(), messageCollection);
        recycler_view_message.setAdapter(messageAdapter);
    }

    public void setOnUiArguments(final Bundle bundle) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String contactName = bundle.getString("NTH_CONTACT_NAME");
                String phoneNumber = bundle.getString("NTH_PHONE_NUMBER");
                String message = bundle.getString("NTH_MESSAGE");
                String photoUri = bundle.getString("NTH_PHOTO");

                messageCollection.add(new Message(contactName, phoneNumber, message, Uri.parse(photoUri)));
                messageAdapter.notifyDataSetChanged();
            }
        });
    }
}
