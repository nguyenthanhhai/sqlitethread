package org.stackqueue.sqlitethread.databasespecific;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseOpenHelper extends SQLiteOpenHelper {
    public DatabaseOpenHelper(Context context) {
        super(context, "Address", null, 1);
    }

    public DatabaseOpenHelper(Context context, String name) {
        super(context, name, null, 1);
    }

    public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory) {
        super(context, name, cursorFactory, 1);
    }

    public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        StringBuilder contacts = new StringBuilder();
        contacts.append("create table contacts");
        contacts.append("(");
        contacts.append("contact_pk integer not null,");
        contacts.append("contact_name varchar(256) not null,");
        contacts.append("phone_number varchar(256) not null,");
        contacts.append("constraint pk_contacts primary key(contact_pk)");
        contacts.append(");");
        sqLiteDatabase.execSQL(contacts.toString());

        StringBuilder messages = new StringBuilder();
        messages.append("create table messages");
        messages.append("(");
        messages.append("message_pk integer not null,");
        messages.append("contact_name varchar(256) not null,");
        messages.append("phone_number varchar(256) not null,");
        messages.append("message varchar(256) null,");
        messages.append("constraint pk_messages primary key(message_pk)");
        messages.append(");");
        sqLiteDatabase.execSQL(messages.toString());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("drop table if exists contacts;");
        stringBuilder.append("drop table if exists messages;");

        sqLiteDatabase.execSQL(stringBuilder.toString());
        onCreate(sqLiteDatabase);
    }
}
