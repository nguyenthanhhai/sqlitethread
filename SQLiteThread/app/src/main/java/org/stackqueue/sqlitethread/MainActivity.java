package org.stackqueue.sqlitethread;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.stackqueue.sqlitethread.businessgeneric.IContact;
import org.stackqueue.sqlitethread.businessgeneric.IMessage;
import org.stackqueue.sqlitethread.businessspecific.TContact;
import org.stackqueue.sqlitethread.businessspecific.TMessage;
import org.stackqueue.sqlitethread.databasegeneric.Contact;
import org.stackqueue.sqlitethread.databasegeneric.Message;

public class MainActivity extends AppCompatActivity {
    private EditText edit_text_message;
    private Button button_send;
    private Button button_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if ((ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED)
                && (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_CONTACTS}, 0x00);
        }

        edit_text_message = (EditText) findViewById(R.id.edit_text_message);
        button_send = (Button) findViewById(R.id.button_send);
        button_create = (Button) findViewById(R.id.button_create);

        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction("org.stackqueue.sqlitethread.ACTION_MESSAGE");
                intent.putExtra("NTH", edit_text_message.getText().toString());
                sendBroadcast(intent);
            }
        });

        button_create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IContact iContact = new TContact(MainActivity.this);
                long insertContact = iContact.insertContact(new Contact(0, "Hai Nguyen", "0918041481"));
                Log.i("NTH", String.valueOf(insertContact));

                IMessage iMessage = new TMessage(MainActivity.this);
                long insertMessage = iMessage.insertMessage(new Message(0, "Hai Nguyen", "0918041481", "Sent from hai nguyen"));
                Log.i("NTH", String.valueOf(insertMessage));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
