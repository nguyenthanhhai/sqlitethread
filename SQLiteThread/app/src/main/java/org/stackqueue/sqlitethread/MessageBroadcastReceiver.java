package org.stackqueue.sqlitethread;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.telephony.SmsMessage;

public class MessageBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
            for (SmsMessage smsMessage : smsMessages) {
                Cursor cursor = context.getContentResolver()
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.PHOTO_URI},
                                ContactsContract.CommonDataKinds.Phone.NUMBER + " = ?",
                                new String[]{smsMessage.getOriginatingAddress().replace(smsMessage.getOriginatingAddress().substring(0, 3), "0")},
                                null, null);
                if (cursor.moveToFirst()) {
                    Intent intentViewPager = new Intent(context, ViewPagerActivity.class);
                    Bundle bundleViewPager = new Bundle();
                    bundleViewPager.putString("NTH_CONTACT_NAME", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)));
                    bundleViewPager.putString("NTH_PHONE_NUMBER", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                    bundleViewPager.putString("NTH_MESSAGE", smsMessage.getMessageBody());
                    bundleViewPager.putString("NTH_PHOTO", cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));
                    intentViewPager.putExtra("NTH_BUNDLE", bundleViewPager);
                    context.startActivity(intentViewPager);
                }
                cursor.close();
            }
        }
    }
}
