package org.stackqueue.sqlitethread;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class StartThreadActivity extends AppCompatActivity {
    private TextView text_view_run;
    private Button button_start;

    private Thread myThread = null;
    private boolean isRunning = false;
    private int currentIndex = 0;
    private int nextIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_thread);

        text_view_run = (TextView) findViewById(R.id.text_view_run);
        button_start = (Button) findViewById(R.id.button_start);

        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (myThread == null) {
                    myThread = new Thread(new MyRunnable());
                    myThread.start();
                    isRunning = true;
                } else {
                    isRunning = !isRunning;
                }
            }
        });
    }

    public class MyRunnable implements Runnable {
        @Override
        public void run() {
            for (currentIndex = nextIndex; currentIndex < 65536; currentIndex++) {
                if (isRunning) {
                    Log.i("NTH", String.valueOf(currentIndex));

                    final int finalIndex = currentIndex;
                    nextIndex = currentIndex + 1;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            text_view_run.setText(String.valueOf(finalIndex));
                        }
                    });

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        Log.i("NTH", e.getMessage());
                    }
                } else {
                    myThread = null;
                    return;
                }
            }
        }
    }
}
