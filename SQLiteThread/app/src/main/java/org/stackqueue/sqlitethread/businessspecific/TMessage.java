package org.stackqueue.sqlitethread.businessspecific;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.stackqueue.sqlitethread.businessgeneric.IMessage;
import org.stackqueue.sqlitethread.databasegeneric.Message;
import org.stackqueue.sqlitethread.databasespecific.DatabaseOpenHelper;

import java.util.ArrayList;

public class TMessage extends DatabaseOpenHelper implements IMessage {
    public TMessage(Context context) {
        super(context);
    }

    public TMessage(Context context, String name) {
        super(context, name);
    }

    public TMessage(Context context, String name, SQLiteDatabase.CursorFactory cursorFactory) {
        super(context, name, cursorFactory);
    }

    public TMessage(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public Message fetchMessage(int messagePk) {
        Message message = new Message();

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.query("messages", null, "message_pk = ?", new String[]{String.valueOf(messagePk)}, null, null, null, null);
            if (cursor.moveToFirst()) {
                message.setMessagePk(cursor.getInt(cursor.getColumnIndex("message_pk")));
                message.setContactName(cursor.getString(cursor.getColumnIndex("contact_name")));
                message.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phone_number")));
                message.setMessage(cursor.getString(cursor.getColumnIndex("message")));
            }
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return message;
    }

    @Override
    public ArrayList<Message> fetchMessageCollection() {
        ArrayList<Message> messageCollection = new ArrayList<Message>();

        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.query("messages", null, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                do {
                    Message message = new Message();
                    message.setMessagePk(cursor.getInt(cursor.getColumnIndex("message_pk")));
                    message.setContactName(cursor.getString(cursor.getColumnIndex("contact_name")));
                    message.setPhoneNumber(cursor.getString(cursor.getColumnIndex("phone_number")));
                    message.setMessage(cursor.getString(cursor.getColumnIndex("message")));
                    messageCollection.add(message);
                } while (cursor.moveToNext());
            }
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return messageCollection;
    }

    @Override
    public long insertMessage(Message message) {
        long insert = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("contact_name", message.getContactName());
            contentValues.put("phone_number", message.getPhoneNumber());
            contentValues.put("message", message.getMessage());
            insert = sqLiteDatabase.insert("messages", null, contentValues);
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return insert;
    }

    @Override
    public int updateMessage(Message message) {
        int update = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("contact_name", message.getContactName());
            contentValues.put("phone_number", message.getPhoneNumber());
            contentValues.put("message", message.getMessage());
            update = sqLiteDatabase.update("messages", contentValues, "message_pk = ?", new String[]{String.valueOf(message.getMessagePk())});
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return update;
    }

    @Override
    public int deleteMessage(Message message) {
        int delete = 0;

        try {
            SQLiteDatabase sqLiteDatabase = getWritableDatabase();
            delete = sqLiteDatabase.delete("messages", "message_pk = ?", new String[]{String.valueOf(message.getMessagePk())});
            sqLiteDatabase.close();
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return delete;
    }

    @Override
    public long[] insertMessageCollection(ArrayList<Message> messageCollection) {
        long[] inserts = new long[messageCollection.size()];

        try {
            for (Message message : messageCollection) {
                inserts[messageCollection.indexOf(message)] = insertMessage(message);
            }
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return inserts;
    }

    @Override
    public int[] updateMessageCollection(ArrayList<Message> messageCollection) {
        int[] updates = new int[messageCollection.size()];

        try {
            for (Message message : messageCollection) {
                updates[messageCollection.indexOf(message)] = updateMessage(message);
            }
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return updates;
    }

    @Override
    public int[] deleteMessageCollection(ArrayList<Message> messageCollection) {
        int[] deletes = new int[messageCollection.size()];

        try {
            for (Message message : messageCollection) {
                deletes[messageCollection.indexOf(message)] = deleteMessage(message);
            }
        } catch (Exception exception) {
            Log.i("TMessage", exception.getMessage());
        }

        return deletes;
    }
}
